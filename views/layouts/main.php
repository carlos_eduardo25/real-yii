<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use kartik\icons\FontAwesomeAsset;

FontAwesomeAsset::register($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="icofont/icofont.min.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="img/building-alt.svg" width="80px"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            [
                'label' => 'Início',
                'url' => Yii::$app->homeUrl
            ],
            [
                'label' => 'Administroras',
                'items' => [
                    [
                        'label' => 'Listar', 
                        'url' => ['adm/list-admin']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['adm/admin']
                    ]
                ],
            ],
            [
                'label' => 'Condomínios',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['condo/list-condominio']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['condo/condominio']
                    ]
                ]

            ],
            [
                'label' => 'Blocos',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['bloco/list-bloco']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['bloco/bloco']
                    ]
                ]

            ],
            [
                'label' => 'Unidades',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['unidade/list-unidade']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['unidade/unidade']
                    ]
                ]

            ],
            [
                'label' => 'Moradores',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['morador/clientes']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['morador/cadastro']
                    ]
                ]

            ],
            [
                'label' => 'Conselhos',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['conselho/list-sindico']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['conselho/sindico']
                    ]
                ]
            ],
            [
                'label' => 'Pets',
                'items' => [
                    [
                        'label' => 'Listar',
                        'url' => ['pet/list-pet']
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['pet/pet']
                    ]
                ]
            ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Logout (' .Yii::$app->user->identity->usuario . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 mt-5">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer>
        <div class="info">
            <span>&copy; Todos os direitos reservados.</span> 
        </div>

        <div class="support">
            <a href="#">(47) 90123-4567</a> 
        </div>

        <div class="version float-right">
            <small class="versionControl">v 1.0</small>
        </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
