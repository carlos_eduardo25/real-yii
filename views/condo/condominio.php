<?
use yii\helpers\Url;
use app\controllers\AdmController;
use app\components\estadosComponent;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Condomínio</h1>
</center>
    <form action="<?echo Url::to(['condo/realiza-cadastro-condo']);?>" method="POST" class="formCondo">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="from_adm" class="custom-select" required>
                    <option value="">Selecione a admin...</option>
                    <?
                    foreach(AdmController::listAdminSelect() as $adm){
                    ?>
                    <option value="<?=$adm['id']?>"><?=$adm['nome_adm']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="qtBloco" value="" required placeholder="Blocos">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="nomeCondo" value="" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="logradouro" value="" required placeholder="Logradouro">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="numero" value="" required placeholder="N°">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="bairro" value="" required placeholder="Bairro">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="cidade" value="" required placeholder="Cidade">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="estado" class="custom-select" required>
                    <option value="">Selecione um estado...</option>
                    <?
                    foreach(estadosComponent::estados() as $sig=>$uf){
                    ?>
                    <option value="<?=$sig?>"><?=$uf?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4">
                <input type="text" class="form-control cep" name="cep" value="" required placeholder="CEP">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>