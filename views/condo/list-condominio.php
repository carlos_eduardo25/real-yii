<?

use app\components\alertComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\mascaraComponent;
use app\components\modalComponent;

$url_site = Url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}

?>


<center class="mt-4 pb-4">
    <h1 class="col-12">Condomínios</h1>
</center>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table mt-3" id="listaCondominios">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">Blocos</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Logradouro</th>
                    <th scope="col">N°</th>
                    <th scope="col">Bairro</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">Estado</th>
                    <th scope="col">CEP</th>
                    <th scope="col">Admin</th>
                    <th scope="col">DT. Cadastro</th>
                    <th align="center"><a href="index.php?r=condo%2Fcondominio" class="btn btn-info btn-sm">ADICIONAR</a></th>
                    </tr>
                </thead>
                <tbody>
                    <? 

                    foreach($condominios as $dados){
                        
                    ?>
                    <tr data-id="<?=$dados['id']?>">
                    <td><?=$dados['qtBloco']?></td>
                    <td><?=$dados['nomeCondo']?></td>
                    <td><?=$dados['logradouro']?></td>
                    <td><?=$dados['numero']?></td>
                    <td><?=$dados['bairro']?></td>
                    <td><?=$dados['cidade']?></td>
                    <td><?=$dados['estado']?></td>
                    <td><?=mascaraComponent::mascara($dados['cep'],'cep')?></td>
                    <td><?=$dados['from_adm']?></td>
                    <td><?=Yii::$app->formatter->format($dados['dataCadastro'],'date')?></td>
                    <td>
                        <a style="padding-right: 25px;" href="<?=$url_site?>/index.php?r=condo/deleta-condo&id=<?=$dados['id']?>" class="removerCondominio"><i class="icofont-ui-delete botao"></i></a>
                        <a class="openModal" href="<?=$url_site?>/index.php?r=condo/edita-condominio&id=<?=$dados['id']?>"><i class="icofont-edit botao"></i></a>
                    </td>
                    </tr>
                    <?}?>
                    <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="1">&nbsp;</td>
                    <td colspan="2" align="right" class="totalRegistros">Total Registros: <?=$paginacao->totalCount?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <?= LinkPager::widget(
        [
            'pagination' => $paginacao,
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ]
        ]
    ) ?>
</div>
<?=modalComponent::initModal();?>