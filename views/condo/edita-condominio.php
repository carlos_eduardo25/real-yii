<?
use yii\helpers\Url;
use app\controllers\AdmController;
use app\components\estadosComponent;
use app\components\selectedComponent;
use app\components\mascaraComponent;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Edição de Condomínio</h1>
</center>
    <form action="<?echo Url::to(['condo/edita-cadastro-condo']);?>" method="POST" class="formCondo">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="from_adm" class="custom-select" required>
                    <option value="">Selecione a admin...</option>
                    <?
                    foreach(AdmController::listAdminSelect() as $adm){
                    ?>
                    <option value="<?=$adm['id']?>"<?=selectedComponent::isSelected($adm['id'], $edit['from_adm'])?>><?=$adm['nome_adm']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="qtBloco" value="<?=$edit['qtBloco']?>" required placeholder="Blocos">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="nomeCondo" value="<?=$edit['nomeCondo']?>" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="logradouro" value="<?=$edit['logradouro']?>" required placeholder="Logradouro">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="numero" value="<?=$edit['numero']?>" required placeholder="N°">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="bairro" value="<?=$edit['bairro']?>" required placeholder="Bairro">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="cidade" value="<?=$edit['cidade']?>" required placeholder="Cidade">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="estado" class="custom-select" required>
                    <option value="">Selecione um estado...</option>
                    <?
                    foreach(estadosComponent::estados() as $sig=>$uf){
                    ?>
                    <option value="<?=$sig?>"<?=selectedComponent::isSelected($sig, $edit['estado'])?>><?=$uf?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4">
                <input type="text" class="form-control cep" name="cep" value="<?=mascaraComponent::mascara($edit['cep'],'cep')?>" required placeholder="CEP">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <input type="hidden" name="id" value="<?=$edit['id']?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6 mb-2" type="submit">Salvar</button>
                </center>
            </div>
        </div>
    </form>