<?

use app\components\selectedComponent;
use yii\helpers\Url;
use app\controllers\CondoController;
use app\controllers\BlocoController;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Edição de Unidade</h1>
</center>
    <form action="<?echo Url::to(['unidade/edita-cadastro-unidade']);?>" method="POST" class="formUnidade">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="condoUni" class="fromCondominio custom-select" required>
                    <option value="">Selecione o condomínio...</option>
                    <?
                    foreach(CondoController::listCondoSelect() as $condo){
                    ?>
                    <option value="<?=$condo['id']?>"<?=selectedComponent::isSelected($condo['id'], $edit['condoUni'])?>><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="blocoUni" class="fromBloco custom-select" required>
                    <option value="">Selecione o bloco...</option>
                        <?
                        foreach(BlocoController::listBlocoSelectEdit($edit['condoUni']) as $bloco){
                        ?>
                        <option value="<?=$bloco['id']?>"<?=selectedComponent::isSelected($bloco['id'], $edit['blocoUni'])?>><?=$bloco['nomeBloco']?></option>
                        <?}?>

                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="numUnidade" value="<?=$edit['numUnidade']?>" required placeholder="N° Unidade">
            </div>
            <div class="col-12 col-md-6 mb-3">
                <input type="text" class="form-control" name="metragem" value="<?=$edit['metragem']?>" required placeholder="Metragem">
            </div>
            <div class="col-12 col-md-6 mb-3">
                <input type="text" class="form-control" name="garagem" value="<?=$edit['garagem']?>" required placeholder="Garagens">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <input type="hidden" name="id" value="<?=$edit['id']?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6 mb-2" type="submit">Salvar</button>
                </center>
            </div>
        </div>
    </form>