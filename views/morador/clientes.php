<?
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\mascaraComponent;
use app\components\alertComponent;
use app\components\modalComponent;

$url_site = Url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}

//echo alertComponent::myAlert('success', 'Testando o component','?r=morador%2Fcadastro');

?>

<center>
<h1>Moradores</h1>
</center>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table mt-3" id="listaClientes">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Gênero</th>
                    <th scope="col">CPF</th>
                    <th scope="col">Nascimento</th>
                    <th scope="col">Email</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Unidade</th>
                    <th scope="col">Bloco</th>
                    <th scope="col">Condomínio</th>
                    <th scope="col">DT. Cadastro</th>
                    <th align="center"><a href="index.php?r=morador%2Fcadastro" class="btn btn-info btn-sm">ADICIONAR</a></th>
                    </tr>
                </thead>
                <tbody>
                    <? 

                    foreach($moradores as $dados){
                        
                    ?>
                    <tr data-id="<?=$dados['id']?>">
                    <td><?=$dados['nome']?></td>
                    <td><?=$dados['genero']?></td>
                    <td style="white-space: nowrap;"><?=mascaraComponent::mascara($dados['cpf'],'cpf')?></td>
                    <td><?=$dados['nascimento']?></td>
                    <td><?=$dados['email']?></td>
                    <td style="white-space: nowrap;"><?=mascaraComponent::mascara($dados['telefone'],'telefone')?></td>
                    <td><?=$dados['from_unidade']?></td>
                    <td style="white-space: nowrap;"><?=$dados['from_bloco']?></td>
                    <td><?=$dados['from_condominio']?></td>
                    <td><?=Yii::$app->formatter->format($dados['dataCadastro'],'date')?></td>
                    <td>
                        <a style="padding-right: 25px;" href="<?=$url_site?>/index.php?r=morador/deleta-morador&id=<?=$dados['id']?>" class="removerCliente"><i class="icofont-ui-delete botao"></i></a>
                        <a class="openModal" href="<?=$url_site?>/index.php?r=morador/edita-morador&id=<?=$dados['id']?>"><i class="icofont-edit botao"></i></a>
                    </td>
                    </tr>
                    <?}?>
                    <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="4">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2" align="right" class="totalRegistros">Total Registros: <?=$paginacao->totalCount?></td>
                    </tr>
                </tbody>
            </table>
        </div>    
    </div>
</div>
<div class="row">
    <?= LinkPager::widget(
        [
            'pagination' => $paginacao,
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ]
        ]
    ) ?>
</div>
<?=modalComponent::initModal();?>