<?

use yii\helpers\Url;
use app\components\generoComponent;
use app\controllers\BlocoController;
use app\controllers\CondoController;
use app\controllers\UnidadeController;
use app\components\selectedComponent;
use app\components\mascaraComponent;

?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Edição de Morador</h1>
</center>
    <form action="<?= Url::to(['morador/edita-cadastro-morador']);?>" method="POST" class="formMorador">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="from_condominio" class="fromCondominio custom-select" required>
                    <option value="">Selecione o condomínio...</option>
                    <?
                    foreach(CondoController::listCondoSelect() as $condo){
                    ?>
                    <option value="<?=$condo['id']?>"<?=selectedComponent::isSelected($condo['id'], $edit['from_condominio'])?>><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="from_bloco" class="fromBloco custom-select" required>
                        <option value="">Selecione o bloco...</option>
                        <?
                        foreach(BlocoController::listBlocoSelectEdit($edit['from_condominio']) as $bloco){
                        ?>
                        <option value="<?=$bloco['id']?>"<?=selectedComponent::isSelected($bloco['id'], $edit['from_bloco'])?>><?=$bloco['nomeBloco']?></option>
                        <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="from_unidade" class="fromUnidade custom-select" required>
                    <option value="">Selecione a unidade...</option>
                    <?
                    foreach(UnidadeController::listUnidadeSelectEdit($edit['from_bloco']) as $uni){
                    ?>
                    <option value="<?=$uni['id']?>"<?=selectedComponent::isSelected($uni['id'], $edit['from_unidade'])?>><?=$uni['numUnidade']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="nome" value="<?=$edit['nome']?>" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <?=generoComponent::genSelect('genero',$edit['genero'])?>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control cpf" name="cpf" value="<?=mascaraComponent::mascara($edit['cpf'],'cpf')?>" required placeholder="CPF">
            </div>
            <div class="col-12 col-md-1 mb-3">
                <label class="mt-2" for="nascimento">Nascimento</label>
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input class="form-control" type="date" name="nascimento" value="<?=$edit['nascimento']?>" required placeholder="Nascimento">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="email" value="<?=$edit['email']?>" required placeholder="Email">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="telefone" value="<?=mascaraComponent::mascara($edit['telefone'],'telefone')?>" required placeholder="Telefone">
            </div>
            <div class="col-12 col-md-3 mb-3">
                <?
                // echo FileInput::widget([
                //     'name' => 'files',
                //     'attribute' => 'files[]',
                //     'options' => ['nultiple' => true]
                // ]);
                ?>
            </div>

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <input type="hidden" name="id" value="<?=$edit['id']?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6 mb-2" type="submit">Salvar</button>
                </center>
            </div>
        </div>
    </form>