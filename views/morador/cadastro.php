<?

use yii\helpers\Url;
use app\components\generoComponent;
use app\components\modalComponent;
use app\controllers\CondoController;

?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Morador</h1>
</center>
    <form action="<?= Url::to(['morador/realiza-cadastro-morador']);?>" method="POST" class="formMorador">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="from_condominio" class="fromCondominio custom-select" required>
                    <option value="">Selecione o condomínio...</option>
                    <?
                    foreach(CondoController::listCondoSelect() as $condo){
                    ?>
                    <option value="<?=$condo['id']?>"><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="from_bloco" class="fromBloco custom-select" required>

                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="from_unidade" class="fromUnidade custom-select" required>

                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="nome" value="" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-4 mb-3 genero">
                <?=generoComponent::genSelect('genero')?>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control cpf" name="cpf" value="" required placeholder="CPF">
            </div>
            <div class="col-12 col-md-1 mb-3">
                <label class="mt-2" for="nascimento"><span class="badge badge-secondary p-2">Nascimento</span></label>
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input class="form-control" type="date" name="nascimento" value="" required placeholder="Nascimento">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="email" value="" required placeholder="Email">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control telefone" name="telefone" value="" required placeholder="Telefone">
            </div>        

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>
<?=modalComponent::initModal();?>