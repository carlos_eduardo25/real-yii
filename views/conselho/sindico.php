<?
use yii\helpers\Url;
use app\controllers\CondoController;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Conselho</h1>
</center>
    <form action="<?echo Url::to(['conselho/realiza-cadastro-conselho']);?>" method="POST" class="formConselho">
        <div class="row">
            <div class="col-12 col-md-3 mb-3">
                <select name="condoSindico" class="custom-select" required>
                    <option value="">Selecione o condomínio...</option>
                    <?
                    foreach(CondoController::listCondoSelect() as $condo){
                    ?>
                    <option value="<?=$condo['id']?>"><?=$condo['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input type="text" class="form-control" name="sindico" value="" required placeholder="Síndico">
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input type="text" class="form-control" name="subSindico" value="" required placeholder="Sub Síndico">
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input type="text" class="form-control" name="conselheiro" value="" required placeholder="Conselheiro">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>