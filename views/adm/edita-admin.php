<?
use yii\helpers\Url;
use app\components\mascaraComponent;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Edição de Administradora</h1>
</center>
    <form action="<?echo Url::to(['adm/edita-cadastro-admin']);?>" method="POST" class="formAdmin">
        <div class="row">
            <div class="col-12 col-md-6 mb-3">
                <input type="text" class="form-control" name="nome_adm" value="<?=$edit['nome_adm']?>" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-6 mb-3">
                <input type="text" class="form-control cnpj" name="cnpj" value="<?=mascaraComponent::mascara($edit['cnpj'],'cnpj')?>" required placeholder="CNPJ">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <input type="hidden" name="id" value="<?=$edit['id']?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6 mb-2" type="submit">Salvar</button>
                </center>
            </div>
        </div>
    </form>