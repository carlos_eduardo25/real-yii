<?
use yii\helpers\Url;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Administradora</h1>
</center>
    <form action="<?echo Url::to(['adm/realiza-cadastro-admin']);?>" method="POST" class="formAdmin">
        <div class="row">
            <div class="col-12 col-md-6 mb-3">
                <input type="text" class="form-control" name="nome_adm" value="" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-6 mb-3">
                <input type="text" class="form-control cnpj" name="cnpj" value="" required placeholder="CNPJ">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>