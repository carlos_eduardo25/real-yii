<?
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\mascaraComponent;
use app\components\modalComponent;
use app\components\alertComponent;

$url_site = Url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}

?>

<center>
<h1>Administradoras</h1>
</center>

<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table mt-3" id="listaAdmins">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">CNPJ</th>
                    <th scope="col">DT. Cadastro</th>
                    <th align="center"><a href="index.php?r=adm%2Fadmin" class="btn btn-info btn-sm">ADICIONAR</a></th>
                    </tr>
                </thead>
                <tbody>
                    <? 

                    foreach($administradoras as $dados){
                        
                    ?>
                    <tr data-id="<?=$dados['id']?>">
                    <td><?=$dados['nome_adm']?></td>
                    <td><?=mascaraComponent::mascara($dados['cnpj'],'cnpj')?></td>
                    <td><?=Yii::$app->formatter->format($dados['dataCadastro'],'date')?></td>
                    <td>
                        <a style="padding-right: 25px;" href="<?=$url_site?>/index.php?r=adm/deleta-admin&id=<?=$dados['id']?>" class="removerAdmin"><i class="icofont-ui-delete botao"></i></a>
                        <a class="openModal" href="<?=$url_site?>/index.php?r=adm/edita-admin&id=<?=$dados['id']?>"><i class="icofont-edit botao"></i></a>
                    </td>
                    </tr>
                    <?}?>
                    <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="right" class="totalRegistros">Total Registros: <?=$paginacao->totalCount?></td>
                    </tr>
                </tbody>
            </table>
        </div>    
    </div>
</div>
<div class="row">
    <?= LinkPager::widget(
        [
            'pagination' => $paginacao,
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ]
        ]
    ) ?>
</div>
<?=modalComponent::initModal();?>