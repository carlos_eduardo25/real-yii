<?

use app\components\alertComponent;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/site.css">
    <link rel="stylesheet" href="icofont/icofont.min.css">
    <title>Projeto | Login</title>
</head>
<body class="login" style="background-color: #212529;">
    
    <main class="container">
        <div class="row">
            <div class="col-4">
                <center>
                    <form class="form-signin" method="POST" action="<?=Url::to(['site/login']);?>">
                        <i class="icofont-building-alt icofont-10x logo" style="padding-right: 20px; color:grey;"></i>
                        <h1 class="h3 mb-3 font-weight-normal" style="color:grey">Efetue seu login</h1>
                        <label for="usuario" class="sr-only">Usuário</label>
                        <input type="text" id="usuario" class="form-control mb-1" placeholder="Usuário" name="usuario" required autofocus>
                        <label for="inputPassword" class="sr-only">Senha</label>
                        <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Senha" required>
                        
                        <input type="hidden" name="<?= \yii::$app->request->csrfParam?>" value="<?= \yii::$app->request->csrfToken;?>">
                        <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Entrar</button>
                    </form>
                    <br>
                    <?=(isset($_GET['myAlert']) ? alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg'], $_GET['myAlert']['redir']) : '')?>
                </center>
            </div>
        </div>
        
    </main>


    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/app.js"></script>
    <? if(isset($_GET['msg'])){?>
    <script type="text/javascript">
        $(function(){
            myAlert('danger', '<?=$_GET['msg']?>', 'main');
        })
    </script>
    <?}?>
</body>
</html>