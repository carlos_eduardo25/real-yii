<?

use app\controllers\AdmController;
use app\controllers\CondoController;
use app\modules\api\controllers\CondominioController;

?>

<script
src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js">
</script>

<center class="mt-4 pb-4">
    <h1 class="col-12">Sistema de Condomínio</h1>
</center>

<div class="jumbotron">
    <div class="row">
        <div class="col-12 col-md-6 ulOut">
            <ul class="list-group homeTable">
                <li class="text-center list-group-item list-group-item-dark"><b>Condomínios | | Total: <?=CondoController::TotalCondos()?></b></li>
                <?foreach(CondoController::NomesCondos() as $condo){?>
                    <li style="background-color: #343a40; color:white;" class="text-center list-group-item"><?=$condo['nomeCondo']?></li>
                <?}?>
            </ul>
        </div>
        <div class="col-12 col-md-6 ulOut">
            <ul class="list-group homeTable">
                <li class="text-center list-group-item list-group-item-dark"><b>Administradoras | | Total: <?=AdmController::TotalAdmins()?></b></li>
                <?foreach(AdmController::NomesAdmins() as $adm){?>
                    <li style="background-color: #343a40; color:white;" class="text-center list-group-item"><?=$adm['nome_adm']?></li>
                <?}?>
            </ul>
        </div>
    </div>

    <div class="row mt-4 mb-3">
        <div class="col-12">
            <h1 class="text-center">Lucros mensais 2022</h1>
            <h4 class="text-center">(Em milhares de reais)</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <canvas id="myChart" style="width:100%;max-width:1000px"></canvas>
        </div>
    </div>
</div>
<script>
var xValues = ["Janeiro","Fevereiro","Março","Abril"];
var yValues = [460,430,480,510];

new Chart("myChart", {
  type: "line",
  data: {
    labels: xValues,
    datasets: [{
      fill: false,
      lineTension: 0,
      backgroundColor: "rgba(0,0,255,1.0)",
      borderColor: "rgba(0,0,255,0.1)",
      data: yValues
    }]
  },
  options: {
    legend: {display: false},
    scales: {
      yAxes: [{ticks: {min: 350, max:550}}],
    }
  }
});
</script>
