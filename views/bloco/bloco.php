<?
use yii\helpers\Url;
use app\controllers\CondoController;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Bloco</h1>
</center>
    <form action="<?echo Url::to(['bloco/realiza-cadastro-bloco']);?>" method="POST" class="formBloco">
        <div class="row">
            <div class="col-12 col-md-3 mb-3">
                <select name="condoBloco" class="custom-select" required>
                    <option value="">Selecione o condomínio...</option>
                    <?
                    foreach(CondoController::listCondoSelect() as $adm){
                    ?>
                    <option value="<?=$adm['id']?>"><?=$adm['nomeCondo']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input type="text" class="form-control" name="nomeBloco" value="" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input type="text" class="form-control" name="andares" value="" required placeholder="N° Andares">
            </div>
            <div class="col-12 col-md-3 mb-3">
                <input type="text" class="form-control" name="unidades" value="" required placeholder="N° Unidades">
            </div>
            

            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>