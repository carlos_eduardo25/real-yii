<?

use app\components\modalComponent;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\alertComponent;

$url_site = Url::base(true);

if(isset($_GET['myAlert'])){
    echo alertComponent::myAlert($_GET['myAlert']['type'],$_GET['myAlert']['msg']);
}

?>

<center>
<h1>Pets</h1>
</center>
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table mt-3" id="listaPets">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">Dono(a)</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Tipo de Animal</th>
                    <th scope="col">DT. Cadastro</th>
                    <th align="center"><a href="index.php?r=pet%2Fpet" class="btn btn-info btn-sm">ADICIONAR</a></th>
                    </tr>
                </thead>
                <tbody>
                    <? 

                    foreach($pets as $dados){
                        
                    ?>
                    <tr data-id="<?=$dados['id']?>">
                    <td><?=$dados['from_morador']?></td>
                    <td><?=$dados['nomePet']?></td>
                    <td><?=$dados['tipo']?></td>
                    <td><?=Yii::$app->formatter->format($dados['dataCadastro'],'date')?></td>
                    <td>
                        <a style="padding-right: 25px;" href="<?=$url_site?>/index.php?r=pet/deleta-pet&id=<?=$dados['id']?>" class="removerPet"><i class="icofont-ui-delete botao"></i></a>
                        <a class="openModal" href="<?=$url_site?>/index.php?r=pet/edita-pet&id=<?=$dados['id']?>"><i class="icofont-edit botao"></i></a>
                    </td>
                    </tr>
                    <?}?>
                    <tr>
                    <td colspan="3">&nbsp;</td>
                    <td colspan="2" align="right" class="totalRegistros">Total Registros: <?=$paginacao->totalCount?></td>
                    </tr>
                </tbody>
            </table>
        </div>    
    </div>
</div>
<div class="row">
    <?= LinkPager::widget(
        [
            'pagination' => $paginacao,
            'linkContainerOptions' => [
                'class' => 'page-item'
            ],
            'linkOptions' => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'class' => 'page-link'
            ]
        ]
    ) ?>
</div>
<?=modalComponent::initModal();?>