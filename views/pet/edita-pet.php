<?

use app\components\selectedComponent;
use yii\helpers\Url;
use app\controllers\MoradorController;
use app\components\animaisComponent;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Edição de Pet</h1>
</center>
    <form action="<?echo Url::to(['pet/edita-cadastro-pet']);?>" method="POST" class="formPet">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="from_morador" class="custom-select" required>
                    <option value="">Selecione o(a) dono(a)...</option>
                    <?
                    foreach(MoradorController::listMoradorSelect() as $mor){
                    ?>
                    <option value="<?=$mor['id']?>"<?=selectedComponent::isSelected($mor['id'], $edit['from_morador'])?>><?=$mor['nome']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="nomePet" value="<?=$edit['nomePet']?>" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="tipo" class="custom-select" required>
                    <option value="">Selecione o tipo do animal...</option>
                    <?
                    foreach(animaisComponent::animais() as $val=>$nome){
                    ?>
                    <option value="<?=$val?>"<?=selectedComponent::isSelected($val, $edit['tipo'])?>><?=$nome?></option>
                    <?}?>
                </select>
            </div>
            
            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <input type="hidden" name="id" value="<?=$edit['id']?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6 mb-2" type="submit">Salvar</button>
                </center>
            </div>
        </div>
    </form>