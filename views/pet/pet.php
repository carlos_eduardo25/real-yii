<?

use app\components\animaisComponent;
use yii\helpers\Url;
use app\controllers\MoradorController;
?>

<center class="mt-4 pb-4">
    <h1 class="col-12">Cadastro de Pet</h1>
</center>
    <form action="<?echo Url::to(['pet/realiza-cadastro-pet']);?>" method="POST" class="formPet">
        <div class="row">
            <div class="col-12 col-md-4 mb-3">
                <select name="from_morador" class="custom-select" required>
                    <option value="">Selecione o morador...</option>
                    <?
                    foreach(MoradorController::listMoradorSelect() as $mor){
                    ?>
                    <option value="<?=$mor['id']?>"><?=$mor['nome']?></option>
                    <?}?>
                </select>
            </div>
            <div class="col-12 col-md-4 mb-3">
                <input type="text" class="form-control" name="nomePet" value="" required placeholder="Nome">
            </div>
            <div class="col-12 col-md-4 mb-3">
                <select name="tipo" class="custom-select" required>
                    <option value="">Selecione o tipo de animal...</option>
                    <?
                    foreach(animaisComponent::animais() as $val=>$nome){
                    ?>
                    <option value="<?=$val?>"><?=$nome?></option>
                    <?}?>
                </select>
            </div>
            
            <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

            <div class="col-12 col-md-12 mt-3">
                <center>
                    <button class="btn btn-info col-6" type="submit">Enviar</button>
                </center>
            </div>
        </div>
    </form>