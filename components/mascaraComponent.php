<?
namespace app\components;

use yii\base\Component;

class mascaraComponent extends Component{

    public static function mascara($val = '',$format){
        $masked = '';
        $key = 0;
        switch ($format) {
            case 'cpf':
                $mask = '###.###.###-##';
                break;

            case 'cnpj':
                $mask = '##.###.###/####-##';
                break;

            case 'cep':
                $mask = '#####-###';
                break;
            
            case 'telefone':
                $mask = (strlen($val) > 10 ? '(##) #####-####' : '(##) ####-####');
                break;
            
            default:
                $mask = null;
                break;
        }
        if($val == null){
            return '--';
        }

        for ($i = 0; $i <= strlen($mask) - 1; ++$i){
            if($mask[$i] == '#'){
                if (isset($val[$key])){
                    $masked .= $val[$key++];
                }
            }
            else if(isset($val[$key])){
                $masked .= $mask[$i];
            }
        }

        return $masked;
    }

    public static function unmask($value){
        $value = str_replace('.','',$value);
        $value = str_replace('-','',$value);
        $value = str_replace('/','',$value);
        $value = str_replace('(','',$value);
        $value = str_replace(')','',$value);
        $value = str_replace(' ','',$value);
        $value = str_replace('+','',$value);

        return $value;
    }
}
?>