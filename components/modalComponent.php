<?
namespace app\components;

use yii\base\Component;

class modalComponent extends Component{

    public static function initModal($title =''){
        $estrutura = "<div class=\"modal fade \" id=\"modalComponent\" data-backdrop=\"static\" data-keyboard=\"false\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog modal-xl\">
            <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" id=\"exampleModalLabel\">{$title}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
                {{injector}}
            </div>
            </div>
        </div>
        </div>";

        return $estrutura;
    }
}
?>