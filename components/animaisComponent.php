<?
namespace app\components;

use yii\base\Component;

class animaisComponent extends Component{

    public static function animais(){
        return array(
            "Cachorro" => "Cachorro",
            "Gato" => "Gato",
            "Pássaro" => "Pássaro",
            "Peixe" => "Peixe",
            "Roedor" => "Roedor",
            "Lagarto" => "Lagarto",
            "Serpente" => "Serpente",
            "Tartaruga" => "Tartaruga",
            "Sapo/Rã" => "Sapo/Rã",
            "Furão" => "Furão",
            "Porco doméstico" => "Porco doméstico",
            "Aracnídeo" => "Aracnídeo",
            "Inseto" => "Inseto",
            "Salamandra" => "Salamandra",
            "Caramujo" => "Caramujo",
            "Caranguejo" => "Caranguejo",
        );
    }
}
?>