<?
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm é o model por trás do login form
 */

class LoginForm extends User{
    
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array regras de validação
     */

    public function rule(){
        return[
            [['usuario','senha'],'required'],
            ['senha','validatePassword']
        ];
    }

    /**valida a senha
     * esse metodo serve como a validaçao inline da senha
     * 
     * @param string
     * @param array
     */

    public function validatePassword($attribute, $params){
        if($this->hasErrors()){
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)){
                $this->addError($attribute, 'Dados não conferem');
            }
        }
    }

    /**
     * Loga o usuario usando user e senha
     */

    public function login(){
        if($this->validate()){
            return Yii::$app->user->login($this->getUser(),$this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * acha usuario pelo [username]
     */
    
    public function getUser(){
        if($this->_user === false){
            $this->_user = User::findOne(['usuario' => $this->username]);
        }
        return $this->_user;
    }
}
?>