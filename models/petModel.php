<?
namespace app\models;

use yii\db\ActiveRecord;

class PetModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'pets';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['nomePet','tipo','from_morador'], 'required'],
        ];
    }
}
?>