<?
namespace app\models;

use phpDocumentor\Reflection\Types\Void_;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface{

    public static function tableName(){
        return 'usuarios';
    }
    /**
     * procura por uma instancia da calsse de identidade usando o ID de user especificado.
     * é usado quando vc precisa manter o login ativo
     */

    public static function findIdentity($id){
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        return null;
    }
    /**
     * retorna o ID do user representador por essa instancia da classe de identidade
     * @return void
     */

    public function getId(){
        return $this->id;
    }
    /**
     * retorna uma chave para verificar login via cookie. A chave é mantida no cookie de login e será comparado com a info do lado do server para testar o cookie
     * 
     * @return void
     */
    public function getAuthKey(){
        return null;
    }
    /**
     * implementa a logica de verificação da chave de login via cookie;
     * 
     * @param [type] $authKey
     * @return void
     */
    public function validateAuthKey($authKey){
        return null;//$this->auth_key === $authKey;
    }
}
?>