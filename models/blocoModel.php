<?
namespace app\models;

use yii\db\ActiveRecord;

class BlocoModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'bloco';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['condoBloco','nomeBloco','andares','unidades'], 'required'],
        ];
    }
}
?>