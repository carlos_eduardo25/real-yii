<?
namespace app\models;

use yii\db\ActiveRecord;

class AdmModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'administradoras';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['nome_adm','cnpj'], 'required'],
        ];
    }
}
?>