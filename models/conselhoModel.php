<?
namespace app\models;

use yii\db\ActiveRecord;

class ConselhoModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'conselho';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['sindico','subSindico','conselheiro','condoSindico'], 'required'],
        ];
    }
}
?>