<?
namespace app\models;

use yii\db\ActiveRecord;

class MoradorModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'moradores';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['nome','cpf','email','genero','telefone','nascimento','from_unidade','from_bloco','from_condominio'], 'required'],
        ];
    }
}
?>