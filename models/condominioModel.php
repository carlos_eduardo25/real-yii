<?
namespace app\models;

use yii\db\ActiveRecord;

class CondominioModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'condominio';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['nomeCondo','from_adm','qtBloco','logradouro','numero','bairro','cidade','estado','cep'], 'required'],
        ];
    }
}
?>