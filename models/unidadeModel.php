<?
namespace app\models;

use yii\db\ActiveRecord;

class UnidadeModel extends ActiveRecord{
    #funcao para retornar a tabela
    public static function tableName(){
        return 'unidade';
    }

    #funcao para determinar que as colunas são obrigatórias a terem dados na hora do cadastro e edicao
    public function rules(){
        return [
            [['condoUni','blocoUni','numUnidade','metragem','garagem'], 'required'],
        ];
    }
}
?>