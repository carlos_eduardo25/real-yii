<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\web\Controller;
use Halfpastfour\PHPChartJS\Chart\Bar;

#Controller padrão do Yii

class SiteController extends Controller{

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(){
        if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        return $this->render('home');
    }

    public function actionLogin(){
        $this->layout = false;
        if(!Yii::$app->user->isGuest){
            return $this->goHome();
        }

        $request = \yii::$app->request;

        if($request->isPost){
            $identity = LoginForm::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
            if($identity){
                Yii::$app->user->login($identity);
                return $this->redirect(['index']);
            }
            else{
                return $this->redirect(['login',
                'myAlert' =>
                [
                    'type' => 'warning',
                    'msg' => 'Errastes os dados',
                    'redir' => 'index.php?r=site/login'
                ]
                ]);
            }
        }
        return $this->render('login');
    }

    public function actionLogout(){
        Yii::$app->user->logout();

        return $this->redirect(['site/login']);
    }

    public function actionChart(){
        $bar = new Bar();
        $bar->setId("myBar");
        
        // Set labels
        $bar->labels()->exchangeArray(["M", "T", "W", "T", "F", "S", "S"]);
        
        // Add apples
        $apples = $bar->createDataSet();
        $apples->setLabel("apples")
        ->setBackgroundColor("rgba( 0, 150, 0, .5 )")
        ->data()->exchangeArray([12, 19, 3, 17, 28, 24, 7]);
        $bar->addDataSet($apples);
        
        // Add oranges as well
        $oranges = $bar->createDataSet();
        $oranges->setLabel("oranges")
        ->setBackgroundColor("rgba( 255, 153, 0, .5 )")
        ->data()->exchangeArray([ 30, 29, 5, 5, 20, 3 ]);
        
        // Add some extra data
        $oranges->data()->append(10);
        
        $bar->addDataSet($oranges);
        
        // Render the chart
        return $this->render($bar);
    }
}
