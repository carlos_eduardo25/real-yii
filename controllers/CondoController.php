<?
namespace app\controllers;

use app\components\alertComponent;
use app\models\AdmModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CondominioModel;
use app\models\ConselhoModel;
use app\components\mascaraComponent;
use Yii;

class CondoController extends Controller{
    #funcao para listar condominios
    public function actionListCondominio(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        #atribui a tabela condominios na var
        $condoTable = CondominioModel::tableName();
        #atribui a tabela admins na var
        $admTable = AdmModel::tableName();

        #realiza a query de busca de dados no BD
        $query = (new \yii\db\Query())
        ->select(
            'condo.id,
            adm.nome_adm as from_adm,
            condo.nomeCondo,
            condo.qtBloco,
            condo.cep,
            condo.logradouro,
            condo.numero,
            condo.bairro,
            condo.cidade,
            condo.estado,
            condo.dataCadastro'
        )
        ->from($condoTable.' condo')
        #faz o inner join da tabela admins para buscar a relacao
        ->innerJoin($admTable.' adm','adm.id = condo.from_adm');

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a lista por id
        $condo = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem
        return $this->render('list-condominio', [
            'condominios' => $condo,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro
    public function actionCondominio(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        return $this->render('condominio');
    }

    #funcao para realizar o cadastro
    public function actionRealizaCadastroCondo(){
        $request = \yii::$app->request;

        #se request for post, atribui os dados do post na model e salva
        if($request->isPost){
            $model = new CondominioModel();
            #tira a mascara do cep no banco de dados
            foreach($request->post() as $field=>$value){
                if($field == 'cep'){
                    $getMani[$field] = mascaraComponent::unmask($value);
                }
                else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['condo/list-condominio']);
        }

        return $this->render('condominio');
    }

    #funcao para listar os condominios em outros cadastros
    public static function listCondoSelect(){
        $query = CondominioModel::find();
        return $query->orderBy('id')->all();
    }

    #funcao para renderizar a edicao
    public function actionEditaCondominio(){
        
        #esconde o layout
        $this->layout = false;

        $request = \yii::$app->request;

        #se request for get, busca o condominio pelo id do get
        if($request->isGet){
            $query = CondominioModel::find();
            $condos = $query->where(['id' => $request->get()])->one();
        }
        #renderiza a edicao
        return $this->render('edita-condominio',[
            'edit' => $condos
        ]);
    }

    #funcao para salvar os dados editados
    public function actionEditaCadastroCondo(){

        $request = \yii::$app->request;

        #se request for post, busca o condominio pelo id e atribui os dados do post nele
        if($request->isPost){

            $model = CondominioModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados baterem com o BD, atualiza
            if($model->update()){
                return $this->redirect(['condo/list-condominio']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['condo/list-condominio', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar condominio
    public function actionDeletaCondo(){
        $request = \yii::$app->request;

        #se request for get, pega o id do condominio no get
        if($request->isGet){
            $model = CondominioModel::findOne($request->get('id'));
            #se pegou o id, deleta
            if($model->delete()){
                return $this->redirect(['condo/list-condominio', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['condo/list-condominio', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
            }
        }
    }

    #funcao para retornar o total de condominios cadastrados
    public static function TotalCondos(){
        $qry = yii::$app->db->createCommand(
            'SELECT COUNT(id) FROM condominio'
        )->queryScalar();
        return $qry;
    }

    #funcao para retornar o nome dos condominios
    public static function NomesCondos(){
        $qry = yii::$app->db->createCommand(
            'SELECT nomeCondo FROM condominio'
        )->queryAll();
        return $qry;
    }
}
?>