<?
namespace app\controllers;

use Yii;
use yii\base\Controller;

class userLogado extends Controller{

    #funcao para verificar se o usuario está logado
    public function isLogado(){
        if(yii::$app->user->isGuest){
           return $this->redirect(['site/login']);
        }
    }
}
?>