<?
namespace app\controllers;

use app\models\CondominioModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\ConselhoModel;
use Yii;

class ConselhoController extends Controller{
    #funcao para listar o conselho
    public function actionListSindico(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        #atribui a tabela conselho na var
        $consTable = ConselhoModel::tableName();
        #atribui a tabela condominio na var
        $condoTable = CondominioModel::tableName();

        #realiza a consulta no BD
        $query = (new \yii\db\Query())
        ->select(
            'cons.id,
            condo.nomeCondo as condoSindico,
            cons.sindico,
            cons.subSindico,
            cons.conselheiro,
            cons.dataCadastro'
        )
        ->from($consTable.' cons')
        ->innerJoin($condoTable.' condo','condo.id = cons.condoSindico');

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a lista por id
        $cons = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem
        return $this->render('list-sindico', [
            'sindicos' => $cons,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro do conselho
    public function actionSindico(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('sindico');
    }

    #funcao para realizar o cadastro de conselho
    public function actionRealizaCadastroConselho(){
        $request = \yii::$app->request;

        #se request for post, atribui os dados do post na model e salva os dados
        if($request->isPost){
            $model = new ConselhoModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['conselho/list-sindico']);
        }

        return $this->render('sindico');
    }

    #funcao para abrir a edicao de conselho
    public function actionEditaConselho(){
        #esconde o layout
        $this->layout = false;
        $request = \yii::$app->request;

        #se request for get, busca o conselho pelo id selecionado no get
        if($request->isGet){
            $query = ConselhoModel::find();
            $cons = $query->where(['id' => $request->get()])->one();
        }

        #renderiza a edicao
        return $this->render('edita-conselho',[
            'edit' => $cons
        ]);
    }

    #funcao para salvar a edicao de conselho
    public function actionEditaCadastroConselho(){

        $request = \yii::$app->request;

        #se request for post, pega o id no post e atribui os dados na model
        if($request->isPost){

            $model = ConselhoModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados baterem com o BD, atualiza
            if($model->update()){
                return $this->redirect(['conselho/list-sindico']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['conselho/list-sindico', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar o conselho
    public function actionDeletaConselho(){
        $request = \yii::$app->request;

        #se request for get, pega o id selecionado no get
        if($request->isGet){
            $model = ConselhoModel::findOne($request->get('id'));
            #se pegou o id, deleta o conselho
            if($model->delete()){
                return $this->redirect(['conselho/list-sindico', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['conselho/list-sindico', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
            }
        }
    }
}
?>