<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\AdmModel;
use app\components\alertComponent;
use app\components\mascaraComponent;
use Yii;

class AdmController extends Controller{
    #funcao para listar admins
    public function actionListAdmin(){
        #verifica se o usuario está logado
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        #busca as admins no BD
        $query = AdmModel::find();

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a lista pelo id
        $adm = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem
        return $this->render('list-admin', [
            'administradoras' => $adm,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro de admin
    public function actionAdmin(){

        #verifica se está logado
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        return $this->render('admin');
    }

    public function actionRealizaCadastroAdmin(){
        $request = \yii::$app->request;

        #se request for post, busca os atributos da model e salva os dados
        if($request->isPost){
            $model = new AdmModel();
            #tira a mascara do cnpj no banco
            foreach($request->post() as $field=>$value){
                if($field == 'cnpj'){
                    $getMani[$field] = mascaraComponent::unmask($value);
                }
                else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['adm/list-admin']);
        }

        return $this->render('admin');
    }

    #funcao para listar as admins existentes em outras telas de cadastro
    public static function listAdminSelect(){
        $query = AdmModel::find();
        return $query->orderBy('id')->all();
    }

    #funcao para abrir a edicao da admin
    public function actionEditaAdmin(){
        #esconde o layout da edicao
        $this->layout = false;

        $request = \yii::$app->request;

        #se request for get, busca as adms e filtra pelo id selecionado no get
        if($request->isGet){
            $query = AdmModel::find();
            $adms = $query->where(['id' => $request->get()])->one();
        }
        #renderiza a tela de edicao da admin
        return $this->render('edita-admin',[
            'edit' => $adms
        ]);
    }

    #funcao para salvar os dados editados da admin
    public function actionEditaCadastroAdmin(){

        $request = \yii::$app->request;

        #se request for post, busca uma admin pelo id, e coloca os atributos do request na model
        if($request->isPost){

            $model = AdmModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados baterem com as tabelas do BD, atualiza a admin e redireciona para a listagem
            if($model->update()){
                return $this->redirect(['adm/list-admin']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['adm/list-admin', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar a admin
    public function actionDeletaAdmin(){
        $request = \yii::$app->request;

        #se request for get, pega o id da admin no get
        if($request->isGet){
            $model = AdmModel::findOne($request->get('id'));

            #se pegou o id, deleta a admin
            if($model->delete()){
                return $this->redirect(['adm/list-admin', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['adm/list-admin', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
            }
        }
    }

    #funcao para retornar o total de administradoras cadastradas
    public static function TotalAdmins(){
        $qry = yii::$app->db->createCommand(
            'SELECT COUNT(id) FROM administradoras'
        )->queryScalar();
        return $qry;
    }

    #funcao para retornar o nome dos administradoras
    public static function NomesAdmins(){
        $qry = yii::$app->db->createCommand(
            'SELECT nome_adm FROM administradoras'
        )->queryAll();
        return $qry;
    }
}
?>