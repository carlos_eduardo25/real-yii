<?
namespace app\controllers;

use app\models\BlocoModel;
use app\models\CondominioModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\UnidadeModel;
use yii;

class UnidadeController extends Controller{
    #funcao para listar as unidades
    public function actionListUnidade(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        #atribui a tabela unidade na var
        $uniTable = UnidadeModel::tableName();
        #atribui a tabela condominio na var
        $condoTable = CondominioModel::tableName();
        #atribui a tabela bloco na var
        $blocoTable = BlocoModel::tableName();

        #faz a consulta no BD
        $query = (new \yii\db\Query())
        ->select(
            'uni.id,
            condo.nomeCondo as condoUni,
            blo.nomeBloco as blocoUni,
            uni.numUnidade,
            uni.metragem,
            uni.garagem,
            uni.dataCadastro'
        )
        ->from($uniTable.' uni')
        ->innerJoin($condoTable.' condo','condo.id = uni.condoUni')
        ->innerJoin($blocoTable.' blo','blo.id = uni.blocoUni');

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a lista pelo id
        $uni = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem
        return $this->render('list-unidade', [
            'unidades' => $uni,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro
    public function actionUnidade(){
        
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        return $this->render('unidade');
    }

    #funcao para salvar o cadastro
    public function actionRealizaCadastroUnidade(){
        $request = \yii::$app->request;

        #se request for post, coloca os dados do post na model e salva os dados
        if($request->isPost){
            $model = new UnidadeModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidade/list-unidade']);
        }

        return $this->render('unidade');
    }

    #funcao para listar as unidades em um select de outro cadastro
    public static function listUnidadeSelect(){
        $query = UnidadeModel::find();
        return $query->orderBy('numUnidade')->all();
    }

    #funcao para listar somente os blocos de determinado condominio em outros cadastros
    public static function listUnidadeSelectEdit($from){
        $query = UnidadeModel::find();

        #filtra somente as unidades do bloco selecionado no select anterior
        $data = $query->where(['blocoUni' => $from])->orderBy('id')->all();
        return $data;
    }

    #API para listar somente as unidades de determinado bloco em outros cadastros
    public function actionListaUnidadesApi(){
        $request = \yii::$app->request;
        $query = UnidadeModel::find();

        #filtra somente as unidades do bloco selecionado no select anterior
        $data = $query->where(['blocoUni' => $request->post()])->orderBy('id')->all();

        #junta as unidades num array e retorna em json
        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['numUnidade'] = $d['numUnidade'];
            $i++;
        }

        return json_encode($dados);
    }

    #funcao para abrir a edicao da unidade
    public function actionEditaUnidade(){
        #esconde o layout
        $this->layout = false;
        $request = \yii::$app->request;

        #se request for get, busca a unidade pelo id do get
        if($request->isGet){
            $query = UnidadeModel::find();
            $unis = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-unidade',[
            'edit' => $unis
        ]);
    }

    #funcao para salvar a edicao da unidade
    public function actionEditaCadastroUnidade(){

        $request = \yii::$app->request;

        #se request for post, busca o id da unidade pelo post e coloca os dados na model
        if($request->isPost){

            $model = UnidadeModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados baterem com o BD, atualiza
            if($model->update()){
                return $this->redirect(['unidade/list-unidade']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['unidade/list-unidade', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar a unidade
    public function actionDeletaUnidade(){
        $request = \yii::$app->request;

        #se request for get, busca a unidade pelo id do get
        if($request->isGet){
            $model = UnidadeModel::findOne($request->get('id'));
            
            #se pegou um id valido, deleta
            if($model->delete()){
                return $this->redirect(['unidade/list-unidade', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['unidade/list-unidade', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
            }
        }
    }
}
?>