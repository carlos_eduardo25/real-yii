<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\BlocoModel;
use app\models\CondominioModel;
use Yii;

class BlocoController extends Controller{
    #funcao para listar os blocos
    public function actionListBloco(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        #atribui a tabela blocos na var
        $blocoTable = BlocoModel::tableName();
        #atribui a tabela condominios na var
        $condoTable = CondominioModel::tableName();

        #realiza a query de busca dos dados no BD
        $query = (new \yii\db\Query())
        ->select(
            'blo.id,
            condo.nomeCondo as condoBloco,
            blo.nomeBloco,
            blo.andares,
            blo.unidades,
            blo.dataCadastro'
        )
        ->from($blocoTable.' blo')
        #faz o inner join da tabela condominios para buscar a relacao
        ->innerJoin($condoTable.' condo','condo.id = blo.condoBloco');

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a lista de blocos pelo id
        $bloco = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem
        return $this->render('list-bloco', [
            'blocos' => $bloco,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro
    public function actionBloco(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        return $this->render('bloco');
    }

    #funcao para realizar o cadastro
    public function actionRealizaCadastroBloco(){
        $request = \yii::$app->request;

        #se request for post, atribui os dados na model e salva
        if($request->isPost){
            $model = new BlocoModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['bloco/list-bloco']);
        }

        return $this->render('bloco');
    }

    #funcao para listar os blocos em outros cadastros
    public static function listBlocoSelect(){
        $query = BlocoModel::find();
        return $query->orderBy('nomeBloco')->all();
    }

    #funcao para listar somente os blocos de determinado condominio em outros cadastros
    public static function listBlocoSelectEdit($from){
        $query = BlocoModel::find();
        
        #filtra somente os blocos do condominio selecionado
        $data = $query->where(['condoBloco' => $from])->orderBy('id')->all();
        return $data;
    }

    #API para listar somente os blocos de determinado condominio em outros cadastros
    public function actionListaBlocosApi(){
        $request = \yii::$app->request;
        $query = BlocoModel::find();
        
        #filtra somente os blocos do condominio selecionado
        $data = $query->where(['condoBloco' => $request->post()])->orderBy('id')->all();

        #junta os blocos num array e retorna em json
        $dados = array();
        $i = 0;
        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeBloco'] = $d['nomeBloco'];
            $i++;
        }

        return json_encode($dados);
    }

    #funcao abrir a edicao do bloco
    public function actionEditaBloco(){
        #esconde o layout
        $this->layout = false;

        $request = \yii::$app->request;
        
        #se request for get, busca os blocos e filtra pelo id selecionado no get
        if($request->isGet){
            $query = BlocoModel::find();
            $blocos = $query->where(['id' => $request->get()])->one();
        }
        #renderiza a edicao
        return $this->render('edita-bloco',[
            'edit' => $blocos
        ]);
    }

    #funcao para salvar os dados editados
    public function actionEditaCadastroBloco(){

        $request = \yii::$app->request;

        #se request for post, busca o bloco pelo id, e coloca os dados do post na model
        if($request->isPost){

            $model = BlocoModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados conferem com o BD, atualiza os dados
            if($model->update()){
                return $this->redirect(['bloco/list-bloco']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['bloco/list-bloco', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar bloco
    public function actionDeletaBloco(){
        $request = \yii::$app->request;

        #se request for get, pega o id do bloco no get
        if($request->isGet){
            $model = BlocoModel::findOne($request->get('id'));

            #se pegou o id, deleta o bloco
            if($model->delete()){
                return $this->redirect(['bloco/list-bloco', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['bloco/list-bloco', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
            }
        }
    }
}
?>