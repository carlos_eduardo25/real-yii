<?
namespace app\controllers;

use app\components\alertComponent;
use app\models\BlocoModel;
use app\models\CondominioModel;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\MoradorModel;
use app\models\UnidadeModel;
use app\components\mascaraComponent;
use yii;

class MoradorController extends Controller{
    #funcao para listar os moradores
    public function actionClientes(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        #atribui a tabela morador na var
        $moraTable = MoradorModel::tableName();
        #atribui a tabela condominio na var
        $condoTable = CondominioModel::tableName();
        #atribui a tabela bloco na var
        $blocoTable = BlocoModel::tableName();
        #atribui a tabela unidade na var
        $uniTable = UnidadeModel::tableName();

        #realiza a consulta no BD
        $query = (new \yii\db\Query())
        ->select(
            'mor.id,
            condo.nomeCondo as from_condominio,
            blo.nomeBloco as from_bloco,
            uni.numUnidade as from_unidade,
            mor.genero,
            mor.nascimento,
            mor.nome,
            mor.cpf,
            mor.email,
            mor.telefone,
            mor.dataCadastro'
        )
        ->from($moraTable.' mor')
        ->innerJoin($condoTable.' condo','condo.id = mor.from_condominio')
        ->innerJoin($blocoTable.' blo','blo.id = mor.from_bloco')
        ->innerJoin($uniTable.' uni','uni.id = mor.from_unidade');

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a listagem pelo id
        $res = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem de moradores
        return $this->render('clientes', [
            'moradores' => $res,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro de moradores
    public function actionCadastro(){

        #verifica o login
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        return $this->render('cadastro');
    }

    #funcao para realizar o cadastro de morador
    public function actionRealizaCadastroMorador(){
        $request = \yii::$app->request;

        #se request for post, atribui os dados do post na model e salva os dados
        if($request->isPost){
            $model = new MoradorModel();
            #tira as mascaras do cpf e telefone do banco de dados
            foreach($request->post() as $field=>$value){
                if($field == 'cpf' || $field == 'telefone'){
                    $getMani[$field] = mascaraComponent::unmask($value);
                }
                else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['morador/clientes']);
        }

        return $this->render('cadastro');
    }

    #funcao para listar os moradores em outros cadastros
    public static function listMoradorSelect(){
        $query = MoradorModel::find();
        return $query->orderBy('id')->all();
    }

    #funcao para abrir a edicao de morador
    public function actionEditaMorador(){
        #esconde o layout
        $this->layout = false;
        $request = \yii::$app->request;

        #se request for get, busca o morador pelo id selecionado no get
        if($request->isGet){
            $query = MoradorModel::find();
            $moras = $query->where(['id' => $request->get()])->one();
        }
        #renderiza a edicao de morador
        return $this->render('edita-morador',[
            'edit' => $moras
        ]);
    }

    #funcao para realizar a edicao de morador
    public function actionEditaCadastroMorador(){

        $request = \yii::$app->request;

        #se request for post, busca o morador pelo id no post, e atribui esses dados no id
        if($request->isPost){

            $model = MoradorModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados baterem com o BD, atualiza
            if($model->update()){
                return $this->redirect(['morador/clientes']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['morador/clientes', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar o morador
    public function actionDeletaMorador(){
        
        try{
            $request = \yii::$app->request;
            
            #busca o id do morador pelo get
            $model = MoradorModel::findOne($request->get('id'));
            #se request for get, deleta o morador
            if($request->isGet){
                $model->delete();
            }
            return $this->redirect(['morador/clientes', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
        }
        
        #se nao, exiba erro
        catch(\Throwable $error){
            return $this->redirect(['morador/clientes', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
        }

    }
}
?>