<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\PetModel;
use app\components\alertComponent;
use app\models\MoradorModel;
use Yii;

class PetController extends Controller{
    #funcao para listar pets
    public function actionListPet(){
        #verifica se o usuario está logado
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }

        #atribui a tabela pets na var
        $petTable = PetModel::tableName();

        #atribui a tabela morador na var
        $morTable = MoradorModel::tableName();

        #busca os pets no BD
        $query = (new \yii\db\Query())
        ->select(
            'pet.id,
            mor.nome as from_morador,
            pet.nomePet,
            pet.tipo,
            pet.dataCadastro
            '
        )
        ->from($petTable.' pet')
        #faz o inner join da tabela moradores para buscar a relacao
        ->innerJoin($morTable.' mor','mor.id = pet.from_morador');

        #paginacao com 6 itens
        $paginacao = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $query->count(),
        ]);

        #ordena a lista pelo id
        $pet = $query->orderBy('id')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();
        
        #renderiza a listagem
        return $this->render('list-pet', [
            'pets' => $pet,
            'paginacao' => $paginacao,
        ]);
    }

    #funcao para renderizar o cadastro de pet
    public function actionPet(){

        #verifica se está logado
        if(yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
        }
        
        return $this->render('pet');
    }

    public function actionRealizaCadastroPet(){
        $request = \yii::$app->request;

        #se request for post, busca os atributos da model e salva os dados
        if($request->isPost){
            $model = new PetModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['pet/list-pet']);
        }

        return $this->render('pet');
    }

    #funcao para abrir a edicao de pet
    public function actionEditaPet(){
        #esconde o layout da edicao
        $this->layout = false;

        $request = \yii::$app->request;

        #se request for get, busca os pets e filtra pelo id selecionado no get
        if($request->isGet){
            $query = PetModel::find();
            $pets = $query->where(['id' => $request->get()])->one();
        }
        #renderiza a tela de edicao do pet
        return $this->render('edita-pet',[
            'edit' => $pets
        ]);
    }

    #funcao para salvar os dados editados do pet
    public function actionEditaCadastroPet(){

        $request = \yii::$app->request;

        #se request for post, busca um pet pelo id, e coloca os atributos do request na model
        if($request->isPost){

            $model = PetModel::findOne($request->post('id'));
            $model->attributes = $request->post();

            #se os dados baterem com as tabelas do BD, atualiza o pet e redireciona para a listagem
            if($model->update()){
                return $this->redirect(['pet/list-pet']);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['pet/list-pet', 'msg' => 'erro']);
            }
        }
    }

    #funcao para deletar o pet
    public function actionDeletaPet(){
        $request = \yii::$app->request;

        #se request for get, pega o id do pet no get
        if($request->isGet){
            $model = PetModel::findOne($request->get('id'));

            #se pegou o id, deleta o pet
            if($model->delete()){
                return $this->redirect(['pet/list-pet', 'myAlert' => ['type' => 'success', 'msg' => 'Registro deletado']]);
            }
            #se nao, exiba erro
            else{
                return $this->redirect(['pet/list-pet', 'myAlert' => ['type' => 'danger', 'msg' => 'Registro não deletado']]);
            }
        }
    }
}
?>