-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for meuprimeirobanco
DROP DATABASE IF EXISTS `meuprimeirobanco`;
CREATE DATABASE IF NOT EXISTS `meuprimeirobanco` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `meuprimeirobanco`;

-- Dumping structure for table meuprimeirobanco.administradoras
DROP TABLE IF EXISTS `administradoras`;
CREATE TABLE IF NOT EXISTS `administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_adm` varchar(50) DEFAULT NULL,
  `cnpj` varchar(50) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.administradoras: ~5 rows (approximately)
DELETE FROM `administradoras`;
/*!40000 ALTER TABLE `administradoras` DISABLE KEYS */;
INSERT INTO `administradoras` (`id`, `nome_adm`, `cnpj`, `dataCadastro`) VALUES
	(1, 'alo', '56456456', '2022-03-30 11:55:56'),
	(2, 'Centurion', '5555555', '2022-03-30 11:55:56'),
	(18, 'antedeguemon', '4234234234234', '2022-04-20 08:50:20'),
	(19, 'antedeguemon', '4234234234234', '2022-04-20 08:50:41'),
	(20, 'aulaJquery', '123', '2022-04-20 09:08:49');
/*!40000 ALTER TABLE `administradoras` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.bloco
DROP TABLE IF EXISTS `bloco`;
CREATE TABLE IF NOT EXISTS `bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condoBloco` int(11) DEFAULT NULL,
  `nomeBloco` varchar(50) DEFAULT NULL,
  `andares` int(11) DEFAULT NULL,
  `unidades` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_bloco_condominio` (`condoBloco`),
  CONSTRAINT `FK_bloco_condominio` FOREIGN KEY (`condoBloco`) REFERENCES `condominio` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.bloco: ~8 rows (approximately)
DELETE FROM `bloco`;
/*!40000 ALTER TABLE `bloco` DISABLE KEYS */;
INSERT INTO `bloco` (`id`, `condoBloco`, `nomeBloco`, `andares`, `unidades`, `dataCadastro`) VALUES
	(78, 21, 'Bloco 1', 2, 2, '2022-04-13 11:07:34'),
	(79, 21, 'Bloco 2', 2, 2, '2022-04-13 11:08:02'),
	(80, 22, 'Bloco 3', 2, 2, '2022-04-13 11:08:13'),
	(81, 22, 'Bloco 4', 2, 2, '2022-04-13 11:08:24'),
	(82, 23, 'Bloco 5', 2, 2, '2022-04-13 11:08:33'),
	(83, 23, 'Bloco 6', 2, 2, '2022-04-13 11:08:39'),
	(84, 24, 'Bloco 7', 2, 2, '2022-04-13 11:08:46'),
	(85, 24, 'Bloco 8', 2, 2, '2022-04-13 11:08:52');
/*!40000 ALTER TABLE `bloco` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.condominio
DROP TABLE IF EXISTS `condominio`;
CREATE TABLE IF NOT EXISTS `condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qtBloco` int(11) NOT NULL DEFAULT 0,
  `nomeCondo` varchar(50) DEFAULT NULL,
  `logradouro` varchar(50) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `cep` varchar(50) DEFAULT NULL,
  `from_adm` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_condominio_administradoras` (`from_adm`),
  CONSTRAINT `FK_condominio_administradoras` FOREIGN KEY (`from_adm`) REFERENCES `administradoras` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.condominio: ~4 rows (approximately)
DELETE FROM `condominio`;
/*!40000 ALTER TABLE `condominio` DISABLE KEYS */;
INSERT INTO `condominio` (`id`, `qtBloco`, `nomeCondo`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `from_adm`, `dataCadastro`) VALUES
	(21, 2, 'Jardim', 'Rua 1', 100, 'Bairro 1', 'Cidade 1', 'AC', '07651453', 2, '2022-04-13 11:03:51'),
	(22, 2, 'Plateau', 'Rua 2', 200, 'Bairro 2', 'Cidade 2', 'AL', '09854321', 2, '2022-04-13 11:04:30'),
	(23, 2, 'Palace', 'Rua 3', 300, 'Bairro 3', 'Cidade 3', 'AM', '09854765', 1, '2022-04-13 11:05:00'),
	(24, 2, 'Garden', 'Rua 4', 400, 'Bairro 4', 'Cidade 4', 'AP', '84532049', 1, '2022-04-13 11:05:34');
/*!40000 ALTER TABLE `condominio` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.conselho
DROP TABLE IF EXISTS `conselho`;
CREATE TABLE IF NOT EXISTS `conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sindico` varchar(50) DEFAULT NULL,
  `subSindico` varchar(50) DEFAULT NULL,
  `conselheiro` varchar(50) DEFAULT NULL,
  `condoSindico` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.conselho: ~4 rows (approximately)
DELETE FROM `conselho`;
/*!40000 ALTER TABLE `conselho` DISABLE KEYS */;
INSERT INTO `conselho` (`id`, `sindico`, `subSindico`, `conselheiro`, `condoSindico`, `dataCadastro`) VALUES
	(25, 'Moyziasdaasd', 'asdas', 'Goualasdasd', 23, '2022-04-13 11:39:38'),
	(26, 'Nyufu', 'Teini', 'Daclo', 22, '2022-04-13 11:39:54'),
	(27, 'Podus', 'Tuaos', 'Bavuova', 23, '2022-04-13 11:40:07'),
	(28, 'Muaos', 'Xaceionn', 'Semacas', 24, '2022-04-13 11:40:20');
/*!40000 ALTER TABLE `conselho` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.convidados
DROP TABLE IF EXISTS `convidados`;
CREATE TABLE IF NOT EXISTS `convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(50) DEFAULT NULL,
  `cpf` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `from_salao` int(11) DEFAULT NULL,
  `from_unidade` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_convidados_reserva_salao_festas` (`from_salao`),
  KEY `FK_convidados_unidade` (`from_unidade`),
  CONSTRAINT `FK_convidados_reserva_salao_festas` FOREIGN KEY (`from_salao`) REFERENCES `reserva_salao_festas` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_convidados_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `unidade` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.convidados: ~0 rows (approximately)
DELETE FROM `convidados`;
/*!40000 ALTER TABLE `convidados` DISABLE KEYS */;
/*!40000 ALTER TABLE `convidados` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.moradores
DROP TABLE IF EXISTS `moradores`;
CREATE TABLE IF NOT EXISTS `moradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `genero` varchar(50) DEFAULT NULL,
  `cpf` varchar(50) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `from_unidade` int(11) DEFAULT NULL,
  `from_bloco` int(11) DEFAULT NULL,
  `from_condominio` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_moradores_bloco` (`from_bloco`),
  KEY `FK_moradores_condominio` (`from_condominio`),
  KEY `FK_moradores_unidade` (`from_unidade`),
  CONSTRAINT `FK_moradores_bloco` FOREIGN KEY (`from_bloco`) REFERENCES `bloco` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_moradores_condominio` FOREIGN KEY (`from_condominio`) REFERENCES `condominio` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_moradores_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `unidade` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.moradores: ~17 rows (approximately)
DELETE FROM `moradores`;
/*!40000 ALTER TABLE `moradores` DISABLE KEYS */;
INSERT INTO `moradores` (`id`, `nome`, `genero`, `cpf`, `nascimento`, `email`, `telefone`, `from_unidade`, `from_bloco`, `from_condominio`, `dataCadastro`) VALUES
	(90, 'Bacao', 'Bovino', '00000000000', '2022-04-14', 'aa@ff', '47900000000', 119, 78, 21, '2022-04-13 11:24:09'),
	(91, 'Geaga', 'F', '00000000000', '2003-09-25', 'aa@ff', '47900000000', 120, 78, 21, '2022-04-13 11:24:09'),
	(92, 'Roial', NULL, '00000000000', NULL, 'aa@ff', '47900000000', 121, 79, 21, '2022-04-13 11:24:09'),
	(93, 'Esgel', NULL, '00000000000', NULL, 'aa@ff', '47900000000', 122, 79, 21, '2022-04-13 11:24:09'),
	(94, 'Vuclaboy', NULL, '00000000000', NULL, 'bb@ff', '47900000000', 123, 80, 22, '2022-04-13 11:24:09'),
	(95, 'Cuan', NULL, '00000000000', NULL, 'bb@ff', '47900000000', 124, 80, 22, '2022-04-13 11:24:09'),
	(96, 'Gufla', NULL, '00000000000', NULL, 'bb@ff', '47900000000', 125, 81, 22, '2022-04-13 11:24:09'),
	(97, 'Beodi', NULL, '00000000000', NULL, 'bb@ff', '47900000000', 126, 81, 22, '2022-04-13 11:24:09'),
	(98, 'Zaon', NULL, '00000000000', NULL, 'cc@ff', '47900000000', 127, 82, 23, '2022-04-13 11:24:09'),
	(99, 'Vihowol', NULL, '00000000000', NULL, 'cc@ff', '47900000000', 128, 82, 23, '2022-04-13 11:24:09'),
	(100, 'Roenfase', NULL, '00000000000', NULL, 'cc@ff', '47900000000', 129, 83, 23, '2022-04-13 11:24:09'),
	(101, 'Vegiais', NULL, '00000000000', NULL, 'cc@ff', '47900000000', 130, 83, 23, '2022-04-13 11:24:09'),
	(102, 'Marae', NULL, '00000000000', NULL, 'dd@ff', '47900000000', 131, 84, 24, '2022-04-13 11:24:09'),
	(103, 'Wepoa', NULL, '00000000000', NULL, 'dd@ff', '47900000000', 132, 84, 24, '2022-04-13 11:24:09'),
	(104, 'Dexio', NULL, '00000000000', NULL, 'dd@ff', '47900000000', 133, 85, 24, '2022-04-13 11:24:09');
/*!40000 ALTER TABLE `moradores` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.pets
DROP TABLE IF EXISTS `pets`;
CREATE TABLE IF NOT EXISTS `pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(50) DEFAULT NULL,
  `tipo` enum('Cachorro','Gato','Passarinho') DEFAULT NULL,
  `from_morador` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_pets_moradores` (`from_morador`),
  CONSTRAINT `FK_pets_moradores` FOREIGN KEY (`from_morador`) REFERENCES `moradores` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.pets: ~0 rows (approximately)
DELETE FROM `pets`;
/*!40000 ALTER TABLE `pets` DISABLE KEYS */;
/*!40000 ALTER TABLE `pets` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.reserva_salao_festas
DROP TABLE IF EXISTS `reserva_salao_festas`;
CREATE TABLE IF NOT EXISTS `reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) DEFAULT NULL,
  `from_unidade` int(11) DEFAULT NULL,
  `dataHora` datetime DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `resp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_reserva_salao_festas_moradores` (`resp`) USING BTREE,
  KEY `FK_reserva_salao_festas_unidade` (`from_unidade`),
  CONSTRAINT `FK_reserva_salao_festas_moradores` FOREIGN KEY (`resp`) REFERENCES `moradores` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_reserva_salao_festas_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `unidade` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.reserva_salao_festas: ~0 rows (approximately)
DELETE FROM `reserva_salao_festas`;
/*!40000 ALTER TABLE `reserva_salao_festas` DISABLE KEYS */;
/*!40000 ALTER TABLE `reserva_salao_festas` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.unidade
DROP TABLE IF EXISTS `unidade`;
CREATE TABLE IF NOT EXISTS `unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condoUni` int(11) DEFAULT NULL,
  `blocoUni` int(11) DEFAULT NULL,
  `numUnidade` int(11) DEFAULT NULL,
  `metragem` float DEFAULT NULL,
  `garagem` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_unidade_bloco` (`blocoUni`) USING BTREE,
  KEY `FK_unidade_condominio` (`condoUni`) USING BTREE,
  CONSTRAINT `FK_unidade_bloco` FOREIGN KEY (`blocoUni`) REFERENCES `bloco` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_unidade_condominio` FOREIGN KEY (`condoUni`) REFERENCES `condominio` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.unidade: ~16 rows (approximately)
DELETE FROM `unidade`;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` (`id`, `condoUni`, `blocoUni`, `numUnidade`, `metragem`, `garagem`, `dataCadastro`) VALUES
	(119, 21, 78, 1, 100, 2, '2022-04-13 11:09:19'),
	(120, 21, 78, 2, 100, 2, '2022-04-13 11:10:05'),
	(121, 21, 79, 3, 100, 2, '2022-04-13 11:10:33'),
	(122, 21, 79, 4, 100, 2, '2022-04-13 11:10:46'),
	(123, 22, 80, 5, 100, 2, '2022-04-13 11:11:04'),
	(124, 22, 80, 6, 100, 2, '2022-04-13 11:11:22'),
	(125, 22, 81, 7, 100, 2, '2022-04-13 11:11:35'),
	(126, 22, 81, 8, 100, 2, '2022-04-13 11:11:45'),
	(127, 23, 82, 9, 100, 2, '2022-04-13 11:11:54'),
	(128, 23, 82, 10, 100, 2, '2022-04-13 11:12:03'),
	(129, 23, 83, 11, 100, 2, '2022-04-13 11:12:10'),
	(130, 23, 83, 12, 100, 2, '2022-04-13 11:12:16'),
	(131, 24, 84, 13, 100, 2, '2022-04-13 11:12:23'),
	(132, 24, 84, 14, 100, 2, '2022-04-13 11:12:30'),
	(133, 24, 85, 15, 100, 2, '2022-04-13 11:12:38'),
	(134, 24, 85, 16, 100, 2, '2022-04-13 11:12:45');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;

-- Dumping structure for table meuprimeirobanco.usuarios
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table meuprimeirobanco.usuarios: ~0 rows (approximately)
DELETE FROM `usuarios`;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(1, 'Carlos', 'carlos25', '123', '2022-04-07 07:35:19');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
