<?
namespace app\modules\api\controllers;

use app\models\PetModel;
use Exception;
use yii\web\Controller;

class PetsController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    #funcao para buscar todos os pets
    public function actionGetAll(){
        $qry = PetModel::find();
        #ordena pelo id
        $data = $qry->orderBy('id')->all();
        $dados = [];
        $i = 0;

        #se buscar, retorna todos os pets e o numero total
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);

    }

    #funcao para buscar um pet
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = PetModel::find();
        #pega o id no get
        $d = $qry->where(['id' => $request->get('id')])->one();

        #se buscar o pet, retorna ele
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $r;

            }

        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }   

        return json_encode($dados);
    }

    #funcao para cadastrar pet
    public function actionRegisterPet(){
        $request = \yii::$app->request;

        #tenta
        try{
            #se request for post, atribui os dados do post na model e salva
            if($request->isPost){
                $dados = [];
                $model = new PetModel();
                $model->attributes = $request->post();
                if($model->save()){
                    $dados['endPoint']['status'] = 'success';
                    $dados['endPoint']['msg'] = 'Registro inserido com sucesso';
                }
                #se nao, exiba erro
                else{
                    $dados['endPoint']['status'] = 'noData';
                    $dados['endPoint']['msg'] = 'Registro nao inserido';
                }
    
                return json_encode($dados);
            }
        }
        #se nao, exiba erro
        catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';

            return json_encode($dados);
        }
    }

    #funcao para editar pet
    public function actionEditPet(){
        $request = \yii::$app->request;
            
        #tenta
        try {
            #se request for post, busca o id do post, atribui os dados na model e atualiza
            if($request->isPost){
                $model = PetModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();
                
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido.';

                return json_encode($dados);
            }
        } 
        #se nao, exiba erro
        catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao inserido';

            return json_encode($dados);
        }
    }

    #funcao para deletar o pet
    public function actionDeletePet(){
        $request = \yii::$app->request;

        #tenta
        try {
            #se request for post, busca o id no post e deleta o pet
            if($request->isPost){
                $model = PetModel::findOne($request->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado com sucesso.';
                
                return json_encode($dados);
            }
        } 
        #se nao, exiba erro
        catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = 'Registro não deletado';

            return json_encode($dados);
        }
    }
}
?>