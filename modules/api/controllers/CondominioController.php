<?
namespace app\modules\api\controllers;

use app\models\CondominioModel;
use Exception;
use yii\web\Controller;

class CondominioController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    #funcao para buscar todos os condominios
    public function actionGetAll(){
        $qry = CondominioModel::find();
        #ordena pelo nome
        $data = $qry->orderBy('nomeCondo')->all();
        $dados = [];
        $i = 0;

        #se buscar mais de 0, retorna os nomes e o numero total
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }

        return json_encode($dados);
    }

    #funcao para buscar um condominio
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = CondominioModel::find();
        #filtra pelo id no get
        $d = $qry->where(['id' => $request->get('id')])->one();

        #se buscar, retorna o nome do condominio
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $r;

            }

        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não existem dados para este consumo.';
        }   

        return json_encode($dados);
    }

    #funcao para pegar os condominios de determinada admin
    public function actionGetCondominioFromAdm(){
        $request = \yii::$app->request;
        $qry = CondominioModel::find();

        #busca o id da admin pelo get e busca os condominios dela
        $data = $qry->where(['from_adm' => $request->get('from_adm')])->orderBy('nomeCondo')->all();
        $dados = [];

        #se buscar, retorna os condominios daquela admin
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeCondo'] = $d['nomeCondo'];
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Sem dados';
        }
        return json_encode($dados);
    }

    #funcao para cadastrar condominio
    public function actionRegisterCondo(){
        $request = \yii::$app->request;

        #tenta
        try {
            #se request for post, atribui os dados do post na model e salva
            if($request->isPost){
                $model = new CondominioModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro adicionado.';

                return json_encode($dados);
            }
        }
        #se nao, exiba erro 
        catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Não tem dados';

            return json_encode($dados);
        }
    }

    #funcao para editar condominio
    public function actionEditCondo(){
        $request = \yii::$app->request;

        #tenta
        try {
            #se request for post, busca o id no post, atribui os dados na model e atualiza
            if($request->isPost){
                $model = CondominioModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado';

                return json_encode($dados);
            }
        }
        #se nao, exiba erro 
        catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao editado';

            return json_encode($dados);
        }
    }

    #funcao para deletar condominio
    public function actionDeleteCondo(){
        $request = \yii::$app->request;

        #tenta
        try {
            #se request for post, busca o id no post e deleta o condominio
            if($request->isPost){
                $model = CondominioModel::findOne($request->post('id'));
                $model->delete();
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado';

                return json_encode($dados);
            }
        }
        #se nao, exiba erro 
        catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = 'Registro não deletado';

            return json_encode($dados);
        }
    }
}
?>