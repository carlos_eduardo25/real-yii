<?
namespace app\modules\api\controllers;

use app\models\BlocoModel;
use yii\web\Controller;

class BlocoController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    #funcao para listar todos os blocos
    public function actionGetAll(){
        $qry = BlocoModel::find();
        #ordena pelo nome
        $data = $qry->orderBy('nomeBloco')->all();
        $dados = [];
        $i = 0;

        #se buscar mais de de 0 blocos, retorna o nome deles e o numero total
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe dado';
        }

        return json_encode($dados);
    }

    #funcao para buscar um bloco
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = BlocoModel::find();
        #filtra pelo id no get
        $d = $qry->where(['id' => $request->get('id')])->one();

        #se buscar o bloco, retorna o nome
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $r;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe dado';
        }
        return json_encode($dados);
    }

    #funcao para pegar o bloco pelo condominio
    public function actionGetBlocoFromCondo(){
        $request = \yii::$app->request;
        $qry = BlocoModel::find();

        #pega o condominio pelo get, e busca os blocos em ordem de nome
        $data = $qry->where(['condoBloco' => $request->get('condoBloco')])->orderBy('nomeBloco')->all();
        $dados = [];

        #se pegar o condominio, retorna todos os blocos dele
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['nomeBloco'] = $d['nomeBloco'];
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Sem dados';
        }
        return json_encode($dados);
    }

    #funcao para cadastrar bloco
    public function actionRegisterBloco(){
        $request = \yii::$app->request;
        #tenta
        try{
            #se request for post, atribui os dados do post na model e salva
            if($request->isPost){
                $model = new BlocoModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido';

                return json_encode($dados);
            }
        }
        #se nao, exiba erro
        catch (\Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe dado';
            
            return json_encode($dados);
        }
    }

    #funcao para editar bloco
    public function actionEditBloco(){
        $request = \yii::$app->request;

        #tenta
        try{
            #se request for post, busca um bloco pelo id no post, atribui os dados na model e atualiza
            if($request->isPost){
                $model = BlocoModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado';

                return json_encode($dados);
            }
        }
        #se nao, exiba erro
        catch (\Exception $th){
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao editado';
            
            return json_encode($dados);
        }
    }

    #funcao para deletar bloco
    public function actionDeleteBloco(){
        $request = \yii::$app->request;

        #tenta
        try {
            #se request for post, busca o id no post e deleta
            if($request->isPost){
                $model = BlocoModel::findOne($request->post('id'));
                $model->delete();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado';

                return json_encode($dados);
            }
        }
        #se nao, exiba erro 
        catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao deletado';

            return json_encode($dados);
        }
    }
}
?>