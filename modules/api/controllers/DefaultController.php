<?
namespace app\modules\api\controllers;

use yii\web\Controller;

class DefaultController extends Controller{
    public function actionIndex(){
        $this->layout = false;
        return $this->render('index');
    }

    #funcao para pegar o token csrf
    public function actionGetTokenPost(){
        $fieldName = \yii::$app->request->csrfParam;
        $tokenValue = \yii::$app->request->csrfToken;

        if($fieldName && $tokenValue){
            $result = [
                'field' => $fieldName,
                'token' => $tokenValue
            ];

            return json_encode($result);
        }
        else{
            return false;
        }
    }
}
?>