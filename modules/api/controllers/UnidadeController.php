<?
namespace app\modules\api\controllers;

use app\models\UnidadeModel;
use yii\web\Controller;

class UnidadeController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }
    
    #funcao para buscar todas as unidades
    public function actionGetAll(){
        $qry = UnidadeModel::find();
        #ordena pelo numero da idade
        $data = $qry->orderBy('numUnidade')->all();
        $dados = [];
        $i = 0;

        #se buscar as unidades, retorna elas e o numero total
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach($data as $d){
                foreach($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Nao existe dados';
        }

        return json_encode($dados);
    }

    #funcao para buscar uma unidade
    public function actionGetOne(){
        $request = \yii::$app->request;
        $qry = UnidadeModel::find();
        $d = $qry->where(['id' => $request->get('id')])->one();

        #se buscar, retorna a unidade
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            foreach($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $d[$r];
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'nao existe dados';
        }

        return json_encode($dados);
    }

    #funcao para buscar as unidades de um bloco especifico
    public function actionGetUnidadeFromBloco(){
        $request = \yii::$app->request;
        $qry = UnidadeModel::find();

        #filtra pega o bloco pelo get e busca todas as unidades dele
        $data = $qry->where(['blocoUni' => $request->get('blocoUni')])->orderBy('numUnidade')->all();
        $dados = [];

        #se buscar, retorna as unidades do bloco selecionado
        if($qry->count() > 0){
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            $i = 0;
            foreach($data as $d){
                $dados['resultSet'][$i]['id'] = $d['id'];
                $dados['resultSet'][$i]['numUnidade'] = $d['numUnidade'];
                $i++;
            }
        }
        #se nao, exiba erro
        else{
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Sem dados';
        }
        return json_encode($dados);
    }

    #funcao para cadastrar a unidade
    public function actionRegisterUnidade(){
        $request = \yii::$app->request;
        
        #tenta
        try {
            #se request for post, atribui os dados do post na model e salva
            if($request->isPost){
                $model = new UnidadeModel();
                $model->attributes = $request->post();
                $model->save();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro inserido';

                return json_encode($dados);
            }
        } 
        #se nao, exiba erro
        catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['status'] = 'nao tem dados';

            return json_encode($dados);
        }
    }

    #funcao para editar unidade
    public function actionEditUnidade(){
        $request = \yii::$app->request;

        #tentar
        try {
            #se request for post, busca o id do post, atribui os dados na model e atualiza
            if($request->isPost){
                $model = UnidadeModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado';

                return json_encode($dados);
            }
        } 
        #se nao, exiba erro
        catch (\Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao editado';

            return json_encode($dados);
        }
    }

    #funcao para deletar a unidade
    public function actionDeleteUnidade(){
        $request = \yii::$app->request;

        #tenta
        try {
            #se request for post, busca o id do post e deleta a unidade
            if($request->isPost){
                $model = UnidadeModel::findOne($request->post('id'));
                $model->delete();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro deletado';

                return json_encode($dados);
            }
        } 
        #se nao, exiba erro
        catch (\Throwable $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = 'Registro nao deletado';

            return json_encode($dados);
        }
    }
}
?>