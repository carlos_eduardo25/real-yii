$(function(){

    
    //FUNCAO EM ANALISE - FILTRA OS DADOS INSERIDOS NO CAMPO DE BUSCA DA LISTAGEM
    // $('#filtro').submit(function(){
    //     var pagina = $('input[name="page"]').val();
    //     var termo1 = $('.termo1').val();
    //     var termo2 = $('.termo2').val();

    //     termo1 = (termo1) ? termo1+'/' : '';
    //     termo2 = (termo2) ? termo2+'/' : '';

    //     window.location.href = url_site+pagina+'/busca/'+termo1+termo2;
    //     return false;
    // })
    
    

    //FUNCAO EM ANALISE - HABILITA/DESABILITA O BOTÃO DE PESQUISA, DEPENDENDO SE O CAMPO DE BUSCA ESTÁ VAZIO OU NAO
    // $('.termo1, .termo2').on('keyup focusout change',function(){
    //     var termo1 = $('.termo1').val();
    //     var termo2 = $('.termo2').val();
    //     if(termo1 || termo2){
    //         $('button[type="submit"]').prop('disabled', false);
    //     }
    //     else{
    //         $('button[type="submit"]').prop('disabled', true);
    //     }
    // })



    //faz com que a nav dropdown ative ao passar o mouse em cima
    $('li.dropdown').hover(function(){
        $(this).find('.dropdown-menu').stop(true,true).toggle('show');
    }, function(){
        $(this).find('.dropdown-menu').stop(true,true).toggle('hide');
    })


    //faz com que, caso o usuario selecione "outros" no campo genero, gera um input text para ele digitar seu genero
    $('.actionGenero').change(function(){
        var gen = $(this).val();
        var name = $(this).attr('id');
        if(gen == 'O'){
            $(this).attr('name','x')
            $(this).parent().append('<input type="text" class="form-control outroGenero" name="'+name+'">')
        }
        else{
            $('.outroGenero').remove();
            $(this).attr('name',name);
        }
    });


    //faz com que o componente modal renderize
    $('.openModal').click(function(){
        caminho = $(this).attr('href');
        $(".modal-body").load(caminho, function (response, status) {
            
            if (status === "success") {
                $('#modalComponent').modal({show: true});
            }
        });
        
        return false;
    })


    //máscaras de input de dados no cadastro
    $(document).ready(function(){
        $('.cpf').mask('000.000.000-00');
        $('.cep').mask('00000-000');
        $('.cnpj').mask('00.000.000/0000-00');
        $('.telefone').mask('(00) 00000-0000');
    })
    
});